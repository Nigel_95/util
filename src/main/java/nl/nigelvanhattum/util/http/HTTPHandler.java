package nl.nigelvanhattum.util.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.stream.Collectors;

public class HTTPHandler {

    /**
     * Does a simple HTTP call. Can only accept Strings as answer
     * @param target targetURL
     * @param method HTTP-method, use enum labels
     * @param message payload
     * @return Answer in String format
     */
    public static String doRequest(String target, String method, String message) {
        String output = null;
        try {

            URL url = new URL(target);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(method);
            //conn.setRequestProperty("Content-Type", "application/json");

            if(!method.equals(HTTPMETHOD.GET.name())) {
                String input = message;
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String line = null;
            output =  br.lines().collect(Collectors.joining());

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return output;
    }

    public static String doGet(String target) {
        return doRequest(target, HTTPMETHOD.GET.name(), "");
    }

    public enum HTTPMETHOD {
        GET, POST, PUT;
    }
}
