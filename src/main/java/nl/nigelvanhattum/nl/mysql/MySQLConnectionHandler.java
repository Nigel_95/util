package nl.nigelvanhattum.nl.mysql;

import java.sql.*;

public class MySQLConnectionHandler {

    /***
     *
     * @param connectionString The complete connectionString. Password, username etc included.
     * @return a java.sql.connection object.
     * @throws SQLException
     */
    public static Connection getConnection(String connectionString) throws SQLException {
        Connection con = DriverManager.getConnection(connectionString);
        return con;
    }

    /***
     * Simple Select query, based on a given connection with a statement
     * @param connection
     * @param selectStatement
     * @return The resultSet of the given statement
     * @throws SQLException
     */
    public static ResultSet doSelect(Connection connection, String selectStatement) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeQuery(selectStatement);
        return statement.getResultSet();
    }
}
